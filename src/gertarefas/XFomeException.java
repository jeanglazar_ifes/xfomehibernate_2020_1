/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gertarefas;

/**
 *
 * @author 1547816
 */
public class XFomeException extends Exception {

    private String tabela[];
    private int codErro;
    
    public XFomeException(String string) {
        super(string);
    }
    
    public XFomeException(int cod) {
        super();
        codErro = cod;
        
        tabela = new String[1000];        
        
        // CLIENTE
        tabela[1] = "Número inválido!";
        tabela[2] = "Número menor que zero!";
        tabela[3] = "CPF inválido!";
        
        // PEDIDO
        tabela[21] = "Erro ao calcular valor total.";
                
    }

    @Override
    public String getMessage() {
        return tabela[codErro];
    }
    
    
 
}

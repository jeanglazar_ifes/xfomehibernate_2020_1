/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gertarefas;

import dao.ClienteDAO;
import dao.ConexaoHibernate;
import dao.GenericDAO;
import dao.PedidoDAO;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Cidade;
import modelo.Cliente;
import modelo.ItemPedido;
import modelo.Lanche;
import modelo.Pedido;
import org.hibernate.HibernateException;

/**
 *
 * @author jean_
 */
public class GerenciadorDominio {
    
    private GenericDAO genDAO;
    private ClienteDAO cliDAO;
    private PedidoDAO pedDAO;

    public GerenciadorDominio() throws HibernateException {
        // TESTE
        ConexaoHibernate.getSessionFactory();
        
        genDAO = new GenericDAO();
        cliDAO = new ClienteDAO();
        pedDAO = new PedidoDAO();
    }
    
    // FUNÇAO GENÉRICA
    public List listar( Class classe) throws HibernateException {
        return genDAO.listar( classe );
    }
    
    public int inserirCliente(String nome, String cpf, Date dtNasc, char sexo,
            String cep, String ender, int num, String complemento, String bairro,
            String referencia, String telFixo, String celular, String email, Icon foto, Cidade cidade) throws HibernateException {

        Cliente cli = new Cliente(nome, cpf, dtNasc, sexo,
                 cep, ender, num, complemento, bairro, referencia, 
                 telFixo, celular, email, FuncoesUteis.IconToBytes(foto), cidade);
        cliDAO.inserir(cli);  
        return cli.getIdCliente();
    }
    
    public void alterarCliente(Cliente cli, String nome, String cpf, Date dtNasc, char sexo,
            String cep, String ender, int num, String complemento, String bairro,
            String referencia, String telFixo, String celular, String email, Icon foto, Cidade cidade) throws HibernateException {

        cli.setNome(nome);
        cli.setCpf(cpf);
        cli.setDtNasc(dtNasc);
        cli.setSexo(sexo);
        cli.getEndereco().setCep(cep);
        cli.getEndereco().setLogradouro(ender);
        cli.getEndereco().setNumero(num);
        cli.getEndereco().setComplemento(complemento);
        cli.getEndereco().setBairro(bairro);
        cli.getEndereco().setReferencia(referencia);
        cli.setTelFixo(telFixo);
        cli.setCelular(celular);
        cli.setEmail(email);
        cli.setFoto(FuncoesUteis.IconToBytes(foto));
        cli.setCidade(cidade);
        
        cliDAO.alterar(cli);  

    }
    
    public void excluir(Object obj ) throws HibernateException {
        genDAO.excluir(obj);
        
    }
    
    public List<Cliente> pesquisarCliente(int tipo, String pesq ) throws HibernateException {
        List<Cliente> lista = null;
        switch ( tipo ) {
            case 0: lista = cliDAO.pesquisarPorNome(pesq); break;
            case 1: lista = cliDAO.pesquisarPorBairro(pesq); break;
            case 2: lista = cliDAO.pesquisarPorMes(pesq); break;   
        }
        return lista;

        // TESTE
//        List<Cliente> lista = cliDAO.listar(Cliente.class);
//        return lista;
 
    }
    
    public int inserirPedido(Cliente cliente, char entregar, JTable tabLanches) throws HibernateException {
         
        float valorTotal = 0;
        Pedido ped = new Pedido(new Date(), entregar, valorTotal, cliente);

        // Percorrer a TABELA de Lanches e insere em PEDIDO na lista de ITEMPEDIDO
        List<ItemPedido> listaItens = ped.getItensPedido();
        
        int tam = tabLanches.getRowCount();
        if (tam > 0) {
            for (int i = 0; i < tam; i++) {                
                int col=0;
                Lanche lan = (Lanche) tabLanches.getValueAt(i, col++);
                int qtde = Integer.parseInt( tabLanches.getValueAt(i, col++).toString() );
                int maisBife = Integer.parseInt( tabLanches.getValueAt(i, col++).toString() );
                int maisQueijo = Integer.parseInt( tabLanches.getValueAt(i, col++).toString() );
                int maisPresunto = Integer.parseInt( tabLanches.getValueAt(i, col++).toString() );
                int maisOvo = Integer.parseInt( tabLanches.getValueAt(i, col++).toString() );
                String ingredientes = tabLanches.getValueAt(i, col++).toString();
                
                ItemPedido item = new ItemPedido(ped, lan, qtde, ingredientes, maisBife, maisOvo, maisPresunto, maisQueijo );
                listaItens.add(item);
                valorTotal = valorTotal + lan.getValor();
            }

            ped.setValorTotal(valorTotal);

            genDAO.inserir(ped);
            return ped.getIdPedido();
        } else {
            return -1;
        }

    }
    
    public List<Pedido> pesquisarPedido(int tipo, String pesq) {
        List<Pedido> lista = null;

        switch (tipo) {
            case 0: 
                lista = pedDAO.pesquisarPorID(pesq);
                break;
            case 1:
                lista = pedDAO.pesquisarPorCliente(pesq);
                break;                
            case 2:
                lista = pedDAO.pesquisarPorBairro(pesq);
                break;
            case 3:
                lista = pedDAO.pesquisarPorMes(pesq);
                break;

        }
        
        return lista;
    }
    
        
    public void relGroupBy(JTable tabela, int tipo) throws Exception {
        List<Object[]> lista = null;
        Cliente cli = null;

        // Limpa a tabela
        ((DefaultTableModel) tabela.getModel()).setRowCount(0);

        switch (tipo) {
            case 'B':
                lista = cliDAO.contPorBairro();
                break;
            case 'M':
                lista = pedDAO.valorPorMes();
                break;
            case 'C':
                lista = pedDAO.valorPorCliente();
                break;
        }

        NumberFormat formato = NumberFormat.getCurrencyInstance();
        // Percorrer a LISTA
        if (lista != null) {

            String col0 = "";
            String col1 = "";
            for (Object[] obj : lista) {
                switch (tipo) {
                    case 'B':
                        col0 = obj[0].toString();
                        col1 = obj[1].toString();
                        break;                        
                    case 'M':
                        col0 = formato.format( Double.parseDouble( obj[0].toString() ));
                        col1 = obj[2].toString() + "/" + obj[1].toString();                        
                        break;
                    case 'C':
                        col0 = formato.format( Double.parseDouble( obj[0].toString() ) );
                        col1 = obj[1].toString();
                        break;
                        
                }

                Object[] novo = new Object[]{col1, col0};
                ((DefaultTableModel) tabela.getModel()).addRow(novo);
            }

        }
    }
    
}









/*



        // TESTE GET e LOAD
        Cliente cli = null;
        int id = Integer.parseInt(pesq);
        switch ( tipo ) {
            case 0: cli = (Cliente) genDAO.get(Cliente.class, id); break;
            case 1: cli = (Cliente) genDAO.load(Cliente.class, id); break;  
        }
        
        List<Cliente> lista = new ArrayList();
        if ( cli != null ) {            
            lista.add(cli);
        }
        return lista;




*/
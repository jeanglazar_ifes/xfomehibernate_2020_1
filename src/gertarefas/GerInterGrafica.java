/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gertarefas;

import gihc.DlgCadCliente;
import gihc.DlgCadLanche;
import gihc.DlgCadPedido;
import gihc.DlgPesqCliente;
import gihc.DlgPesqPedido;
import gihc.DlgRelGroupBy;
import gihc.FrmPrincipal;
import java.awt.Frame;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import modelo.Cliente;
import org.hibernate.HibernateException;

/**
 *
 * @author 1547816
 */
public class GerInterGrafica {

    private FrmPrincipal frmPrinc;
    private DlgCadCliente janCadCli;
    private DlgCadPedido janCadPed;
    private DlgCadLanche janCadLanche;
    private DlgPesqCliente janPesqCliente;
    private DlgPesqPedido janPesqPedido;
    private DlgRelGroupBy janRelGroupBy;
    
    GerenciadorDominio gerDom;
    GerenciadorRelatorios gerRel;
    
    
    public GerInterGrafica() {
        frmPrinc = null;
        janCadCli = null;
        janCadPed = null;
        janPesqCliente = null;
        janPesqPedido = null;
        janRelGroupBy = null;
        
        try {
            gerDom = new GerenciadorDominio();
            gerRel = new GerenciadorRelatorios();
        } catch ( HibernateException ex) {
            JOptionPane.showMessageDialog(frmPrinc, ex );
            System.exit(-1);
        }
        
    }

    public GerenciadorDominio getGerDominio() {
        return gerDom;
    }
  
    public GerenciadorRelatorios getGerRelatorios() {
        return gerRel;
    }
    
    private JDialog abrirJanela(java.awt.Frame parent, JDialog dlg, Class classe){
        if (dlg == null){     
            try {
                dlg = (JDialog) classe.getConstructor(Frame.class, boolean.class, GerInterGrafica.class ).newInstance(parent,true,this);
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                JOptionPane.showMessageDialog(frmPrinc, "Erro ao abrir a janela " + classe.getName() );
            }
        }        
        dlg.setVisible(true);  
        return dlg;
    }
    
    public void janPrincipal() {        
       // abrirJanela(frmPrinc);              
       if (frmPrinc == null){            
            frmPrinc = new FrmPrincipal(this);
        }        
        frmPrinc.setVisible(true); 
    }
    
    public void janCadCliente() {
        // ABRIR a janela de CADASTRO DE CLIENTE                
        abrirJanela(frmPrinc, janCadCli, DlgCadCliente.class );
    }
    
    public Cliente janPesqCliente() {
        janPesqCliente = (DlgPesqCliente) abrirJanela(frmPrinc, janPesqCliente, DlgPesqCliente.class);
        return janPesqCliente.getCliSelecionado();
    }
    
    public void janCadPedido() {
        abrirJanela(frmPrinc, janCadPed, DlgCadPedido.class);
    }
    
    public void janCadLanche() {
        abrirJanela(frmPrinc, janCadLanche, DlgCadLanche.class);
    }
    
    public void janPesqPedido() {
        abrirJanela(frmPrinc, janPesqPedido, DlgPesqPedido.class);
    }
    
    public void janelaRelGroupBy() {
        abrirJanela(frmPrinc, janRelGroupBy, DlgRelGroupBy.class);
    }
    
    
    
    
    

    // FUNÇÃO GENÉRICA
    public void carregarCombo( JComboBox combo, Class classe ) {
        List lista;
        try {
            lista = gerDom.listar(classe);
            combo.setModel( new DefaultComboBoxModel( lista.toArray() ) );
        } catch (HibernateException ex) {
            JOptionPane.showMessageDialog(frmPrinc, "Erro ao carregar COMBO. " + ex );
        }

          
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            JOptionPane.showMessageDialog(null, "Look and Feel não encontrado");
        }
        //</editor-fold>
        

         GerInterGrafica gerIG;
         gerIG = new GerInterGrafica();
         gerIG.janPrincipal();
         
    }
    
}

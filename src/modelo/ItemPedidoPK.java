/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;


/**
 *
 * @author jean_
 */

@Embeddable
public class ItemPedidoPK implements Serializable{
    
    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn(name="idLanche")
    private Lanche lanche;
    
    
    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn(name="idPedido")
    private Pedido pedido;

    public ItemPedidoPK() {
    }

    
    public ItemPedidoPK( Pedido pedido, Lanche lanche) {
        this.lanche = lanche;
        this.pedido = pedido;
    }

    public Lanche getLanche() {
        return lanche;
    }

    public void setLanche(Lanche lanche) {
        this.lanche = lanche;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.lanche);
        hash = 29 * hash + Objects.hashCode(this.pedido);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemPedidoPK other = (ItemPedidoPK) obj;
        if (!Objects.equals(this.lanche, other.lanche)) {
            return false;
        }
        if (!Objects.equals(this.pedido, other.pedido)) {
            return false;
        }
        return true;
    }
    
    
}

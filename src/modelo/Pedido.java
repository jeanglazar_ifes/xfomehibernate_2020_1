/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import gertarefas.FuncoesUteis;
import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;



/**
 *
 * @author 1547816
 */

@Entity
public class Pedido implements Serializable {
    
    @Id
    @GeneratedValue ( strategy = GenerationType.IDENTITY )
    private int idPedido;
    
    @Temporal ( TemporalType.DATE )
    private Date data; 

    @Column (length = 1)
    private char entregar;
    
    private float valorTotal;
    
    @ManyToOne ( fetch = FetchType.EAGER)
    @JoinColumn ( name = "idCliente")
    private Cliente cliente;

    @OneToMany (mappedBy = "chaveComposta.pedido", 
                fetch = FetchType.LAZY, 
                cascade = CascadeType.ALL )
    
    private List<ItemPedido> itensPedido;

    // Construtor VAZIO para o HIBERNATE
    public Pedido() {
        
    }

    public Pedido(Date data, char entregar, float valorTotal, Cliente cliente) {
        this.data = data;
        this.entregar = entregar;
        this.valorTotal = valorTotal;
        this.cliente = cliente;
        this.itensPedido = new ArrayList();
    }

    public Pedido(int idPedido, Date data, char entregar, float valorTotal, Cliente cliente) {
        this.idPedido = idPedido;
        this.data = data;
        this.entregar = entregar;
        this.valorTotal = valorTotal;
        this.cliente = cliente;
        this.itensPedido = new ArrayList();
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public Date getData() {
        return data;
    }
    
    public String getDataStr() {
        try {
            return FuncoesUteis.dateToStr(data);
        } catch (ParseException ex) {
            return "";
        }
    }
  
    public void setData(Date data) {
        this.data = data;
    }


    public char getEntregar() {
        return entregar;
    }

    public void setEntregar(char entregar) {
        this.entregar = entregar;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<ItemPedido> getItensPedido() {
        return itensPedido;
    }

    public void setItensPedido(List<ItemPedido> itensPedido) {
        this.itensPedido = itensPedido;
    }

    
    @Override
    public String toString() {
        return String.valueOf(idPedido);
    }
    
    public Object[] toArray() {   
        
        // FORMATAR MOEDA
        NumberFormat formNum = NumberFormat.getCurrencyInstance();
        
        /*
        // FORMATAR CASAS DECIMAIS 
        DecimalFormat formNum = new DecimalFormat();
        formNum.setMaximumFractionDigits(2);
        formNum.setMinimumFractionDigits(2);
        */
        
        return new Object[] {this, cliente, cliente.getEndereco().getBairro(), 
                             getDataStr(), 
                             formNum.format(valorTotal)  };        
    }
}
